(ns url-matcher.core
  (:require [instaparse.core :as insta]
            [instaparse.combinators :as parsec])
  (:gen-class))

;; TODO: fix limitation, for now it don't work
;; correctly with bind items with same name as parser internal parts items.
;; Workaround - wrap user tags with postfix and unique id (to let user use tags
;; with same name).
;;
;; TODO: we can do URL-encoding for wider matching ability
;;
(def matcher-parser
  "Matcher EBNF grammar defenition.

  Note that not all symbols allowed in URI may be used here.
  Question mark as example."
  (insta/parser
   "EXPRESSION = SPACES HOST? { PATH } { QUERY }
    <SPACES> = <#'\\s'*>
    (* According to https://www.ietf.org/rfc/rfc1738.txt *)
    (* alphanumeric plus $_.+!*'(),- are allowed *)
    (* TODO: check reserved https://tools.ietf.org/html/rfc3986#section-2.2 *)
    <WORD> = #'[a-zA-Z0-9$_.+!*\\'(),-]'*
    MATCH = WORD
    BIND = <'?'> WORD
    <PART> = MATCH | BIND
    (* host *)
    HOST = <'host('> PART <');'> SPACES
    (* path *)
    PATH = <'path('> PART { <'/'> PART } <');'> SPACES
    (* query *)
    QUERY = <'queryparam('> PARAM { <'&'> PARAM } <');'> SPACES
    (* suppose, values in query isn't nested (not sure, could it) *)
    PARAM = KEY [ <'='> VALUE ]
    KEY = PART
    VALUE = PART"))

(defn pattern
  "Recognize matcher for pattern in STRING."
  [string]
  (letfn [(merge-concat [dicts]
            (apply #(apply merge-with concat %&) dicts))]
    (->> string
         matcher-parser
         (insta/transform
          {:BIND #(->> %&
                       clojure.string/join
                       (hash-map :bind))
           :MATCH #(->> %&
                        clojure.string/join
                        (hash-map :match))
           :HOST #(->> %&
                       (hash-map :host))
           :PATH #(->> %&
                       (hash-map :path))
           :KEY #(->> %&
                      (hash-map :key))
           :VALUE #(->> %&
                        (hash-map :value))
           :PARAM #(->> %&
                        merge-concat)
           :QUERY #(->> %&
                        (hash-map :query))
           :EXPRESSION #(->> %&
                             merge-concat
                             (hash-map :pattern))
           })
         :pattern)))

(def uri-bnf
  "URI BNF according to https://tools.ietf.org/html/rfc3986#appendix-A.

  RFC's ABNF transformed to EBNF because of
  parser's ABNF case insensative limitation."
  (parsec/ebnf
   "URI            = URI-absolute / URN

   <URI-absolute>  = URL [ <'#'> <fragment> ]

   <URL>           = <scheme> <':'> hier-part [ <'?'> query ]

   <hier-part>     = <'//'> authority path-abempty
                   / path-absolute
                   / path-rootless
                   / path-empty

   <URN>           = relative-part [ <'?'> query ] [ <'#'> <fragment> ]

   <relative-part> = <'//'> authority path-abempty
                   / authority path-abempty
                   / path-absolute
                   / path-empty

   <scheme>        = ALPHA { ALPHA | DIGIT | '+' | '-' | '.' }

   <authority>     = [ <userinfo> <'@'> ] host [ <':'> <port> ]
   <userinfo>      = { unreserved | pct-encoded | sub-delims | ':' }
   <host>          = <IP-literal / IPv4address / reg-name>
   <port>          = DIGIT*

   <IP-literal>    = '[' ( IPv6address | IPvFuture  ) ']'

   <IPvFuture>     = 'v' HEXDIG+ '.' ( unreserved | sub-delims | ':' )+

   <IPv6address>   =                            ( h16 ':' h16 ':' h16 ':' h16 ':' h16 ':' h16 ':' ) ls32
                   /                       '::' ( h16 ':' h16 ':' h16 ':' h16 ':' h16 ':' ) ls32
                   / [               h16 ] '::' ( h16 ':' h16 ':' h16 ':' h16 ':' ) ls32
                   / [ [ h16 ':' ] h16 ] '::' ( h16 ':' h16 ':' h16 ':' ) ls32
                   / [ [ h16 ':' [ h16 ':' ] ] h16 ] '::' ( h16 ':' h16 ':' ) ls32
                   / [ [ h16 ':' [ h16 ':' [ h16 ':'] ] ] h16 ] '::'    h16 ':'   ls32
                   / [ [ h16 ':' [ h16 ':' [ h16 ':' [ h16 ':' ] ] ] ] h16 ] '::'              ls32
                   / [ [ h16 ':' [ h16 ':' [ h16 ':' [ h16 ':' [ h16 ':' ] ] ] ] ] h16 ] '::'              h16
                   / [ [ h16 ':' [ h16 ':' [ h16 ':' [ h16 ':' [ h16 ':' [ h16 ':'] ] ] ] ] ] h16 ] '::'

   <h16>           = HEXDIG [ HEXDIG [ HEXDIG [ HEXDIG ] ] ]
   <ls32>          = ( h16 ':' h16 ) | IPv4address
   <IPv4address>   = dec-octet '.' dec-octet '.' dec-octet '.' dec-octet

   <dec-octet>     = DIGIT                 (* 0-9 *)
                   | #'[1-9]' DIGIT        (* 10-99 *)
                   | '1' DIGIT DIGIT       (* 100-199 *)
                   | '2' #'[0-4]' DIGIT    (* 200-249 *)
                   | '25' #'[0-5]'         (* 250-255 *)

   <reg-name>      = ( unreserved | pct-encoded | sub-delims )+

   <path-abempty>  = [ <'/'> (path-head? path-rest | segment? path-full) ]
   <path-absolute> = <'/'> [ path-head path-rest | segment path-full ]
   <path-rootless> = path-head path-rest | segment path-full
   <path-empty>    = ''

   <path-segment>  = <'/'> segmentp
   <path-segments> = { path-segment }

   <path-full>     = [ path-segments <'/'> path-head path-rest ]
   <path-head>     = segment
   <path-rest>     = path-segments

   <segmentp>      = segment?
   <segment>       = <pchar+>

   <pchar>         = unreserved | pct-encoded | sub-delims | ':' | '@'

   <query>         = param-head param-rest | parameter param-full

   <parameters>    = { <'&'> parameter }

   <param-full>    = parameter parameters <'&'> param-head param-rest
   <param-head>    = parameter
   <param-rest>    = parameters

   <parameter>     = key [ <'='> value ]
   <key>           = query-segment
   <value>         = query-segment
   <query-segment> = <qchar*>
   <qchar>         = char-na-nq | '/' | '?'
   <char-na-nq>    = unreserved | pct-encoded | delims-na-nq | ':' | '@'

   <fragment>      = { pchar | '/' | '?' }

   <pct-encoded>   = '%' HEXDIG HEXDIG

   <unreserved>    = ALPHA | DIGIT | '-' | '.' | '_' | '~'
   <reserved>      = gen-delims | sub-delims
   <gen-delims>    = ':' | '/' | '?' | '#' | '[' | ']' | '@'
   <delims-na-nq>  = '!' | '$' | \"'\" | '(' | ')'
                   | '*' | '+' | ',' | ';'
   <sub-delims>    = delims-na-nq | '&' | '='

   <HEXDIG>        = DIGIT | 'A' | 'B' | 'C' | 'D' | 'E' | 'F'
                   | 'a' | 'b' | 'c' | 'd' | 'e' | 'f'
   <DIGIT>         = #'[0-9]'
   <ALPHA>         = #'[a-zA-Z]'
"))

(defn pattern-bnf
  "Return dictionary of :matches - BNF list for matched PARTS

  (list of both, match and bind)
  and of :binds - list of bindings keywords."
  [parts]
  (reduce
   #(let [bind (:bind %2)
          bind-key (keyword bind)
          match (if bind
                  (-> bind-key
                      parsec/nt
                      (dissoc :hide)
                      parsec/hide-tag)
                  (-> %2
                      :match
                      parsec/string
                      parsec/hide
                      parsec/hide-tag))
          binds (:binds %)
          matches (:matches %)
          result (assoc % :matches (conj matches match))]
      (if bind
        (assoc result :binds (conj binds bind-key))
        result))
   {:binds []
    :matches []}
   parts))

(defn host-bnf
  "Host BNF for MATCHER."
  [matcher]
  (let [hosts (:host matcher)
        matched-hosts (pattern-bnf hosts)
        host-binds (:binds matched-hosts)
        host-matches (:matches matched-hosts)]
    (apply merge
           (concat
            (map #(hash-map :host %) host-matches)
            (map #(hash-map % (-> uri-bnf
                                  :host
                                  (dissoc :red)
                                  (dissoc :hide))) host-binds)))))

(defn path-bnf
  "Path BNF for MATCHER."
  [matcher]
  (let [segments (:path matcher)
        matched-segments (pattern-bnf segments)
        path-binds (:binds matched-segments)
        path-matches (:matches matched-segments)
        first-segment (first path-matches)
        rest-segments (vec (rest path-matches))]
    (when first-segment
      (apply merge
             (conj
              (map #(hash-map % (-> uri-bnf
                                    :segment
                                    (dissoc :red)
                                    (dissoc :hide))) path-binds)
              {:path-head first-segment
               :path-empty (parsec/nt :path-absolute)
               :path-rest (let [path-segment (:path-segments uri-bnf)]
                            (parsec/hide-tag
                             (apply
                              parsec/cat
                              (reduce #(conj %
                                             (parsec/cat
                                              (-> "/"
                                                  parsec/string
                                                  parsec/hide)
                                              %2
                                              path-segment))
                                      [path-segment]
                                      rest-segments))))})))))

(defn query-parameter-bnf
  "BNF for query PARAMETER."
  [parameter]
  (let [key (:key parameter)
        value (:value parameter)]
    (-> {}
        (cond-> key (assoc :key (pattern-bnf key)))
        (cond-> value (assoc :value (pattern-bnf value))))))

;; Note: order matters for current implementation.
(defn query-bnf
  "Query BNF for MATCHER."
  [matcher]
  (let [queries (:query matcher)
        matched-queries (map query-parameter-bnf queries)
        query-binds (flatten
                     (map
                      #(flatten
                        (reduce
                         (fn [coll items]
                           (let [bind (apply merge (:binds (last items)))]
                             (cond-> coll
                               bind (conj coll bind))))
                         []
                         %))
                      matched-queries))
        query-matches (map
                       #(apply merge
                               (map
                                (fn [items]
                                  (hash-map
                                   (first items)
                                   (apply merge (:matches (last items)))))
                                %))
                       matched-queries)
        parameters-matches (map #(let [value (:value %)
                                       key (:key %)]
                                   (if value
                                     (parsec/cat
                                      key
                                      (-> "="
                                          parsec/string
                                          parsec/hide)
                                      value)
                                     key)) query-matches)
        first-parameter (first parameters-matches)
        rest-parameters (vec (rest parameters-matches))]
    (when first-parameter
      (apply merge
             (conj
              (map #(hash-map % (-> uri-bnf
                                    :query-segment
                                    (dissoc :red)
                                    (dissoc :hide))) query-binds)
              {:param-head (parsec/hide-tag first-parameter)
               :param-rest (let [query-parameter (:parameters uri-bnf)]
                             (parsec/hide-tag
                              (apply
                               parsec/cat
                               (reduce #(conj %
                                              (parsec/cat
                                               (-> "&"
                                                   parsec/string
                                                   parsec/hide)
                                               %2
                                               query-parameter))
                                       [query-parameter]
                                       rest-parameters))))})))))

(defn uri-parser
  "Use parser combinator to dynamically build grammar for MATCHER."
  [matcher & args]
  (apply (insta/parser
          (merge
           uri-bnf
           (host-bnf matcher)
           (path-bnf matcher)
           (query-bnf matcher))
          :start :URI) args))

(defn recognize
  "Recognize patterns in URI."
  [patterns uri]
  (letfn [(merge-concat [dicts]
            (apply #(apply merge-with concat %&) dicts))]
    (->> uri
         (uri-parser patterns)
         (insta/transform
          {:URI #(hash-map :uri %&)})
         :uri
         ((fn [parsed]
            (when parsed
              (->> parsed
                   (map #(vector
                          (first %)
                          (clojure.string/join (rest %))))
                   vec)))))))
