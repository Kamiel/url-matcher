# url-matcher

A Clojure library designed to recognize and destruct URL by:

* host: domain
* path: parts, splitted with "/"
* queryparam: name/value pairs of query

***

Each string that is started from "?" is a "bind"
(recognize matcher) should return nil or seq of binds.


## Installation

    lein install

## Usage

    lein test

## License

Beerware © 2017

Distributed under the Beerware license  either version 42 or (at
your option) any later version.
