(ns url-matcher.core-test
  (:require [clojure.test :refer :all]
            [url-matcher.core :refer :all]))

(def twitter (pattern "host(twitter.com); path(?user/status/?id);"))
(def dribbble (pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);"))
(def dribbble2 (pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);"))

(deftest twitter-test
  (testing "Recognize twitter URL."
    (is (= [[:user "bradfitz"] [:id "562360748727611392"]]
           (recognize twitter "http://twitter.com/bradfitz/status/562360748727611392")))))

(deftest dribbble-test
  (testing "Recognize dribble URL."
    (is (= [[:id "1905065-Travel-Icons-pack"] [:offset "1"]]
           (recognize dribbble "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")))))

(deftest dribbble-nil-test
  (testing "Do not recognize wrong dribble URL."
    (are [uri] (= nil
                  (recognize dribbble uri))
      "https://twitter.com/shots/1905065-Travel-Icons-pack?list=users&offset=1"
      "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users")))

(deftest dribbble2-test
  (testing "Recognize dribble URL with multiple query paremeters."
    (is (= [[:id "1905065-Travel-Icons-pack"] [:offset "1"] [:type "users"]]
           (recognize dribbble2 "https://dribbble.com/shots/1905065-Travel-Icons-pack?offset=1&list=users")))))
